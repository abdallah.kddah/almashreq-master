import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';

import 'app/bloc/app_bloc.dart';
import 'core/constent.dart';
import 'data/db_helper/db_helper.dart';
import 'data/http_helper/http_helper.dart';
import 'data/http_helper/ihttpe_helper.dart';
import 'data/repository/irepository.dart';
import 'data/repository/repository.dart';
import 'data/db_helper/idp_helper.dart';

final sl = GetIt.instance;

Future iniGetIt() async {
  sl.registerLazySingleton(() => ((Dio(BaseOptions(
      baseUrl: BaseUrl,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        "charset": "utf-8",
        "Accept-Charset": "utf-8"
      },
      responseType: ResponseType.plain)))));

  sl.registerLazySingleton<IDbHelper>(() => DbHelper());
  sl.registerLazySingleton<IHttpHelper>(() => HttpHelper());
  sl.registerLazySingleton<IRepository>(() => Repository());

  /// AppBloc

  sl.registerFactory(() => AppBloc(sl()));
}
