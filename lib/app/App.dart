import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maktabeh_app/core/app_language.dart';
import 'package:maktabeh_app/core/app_localizations.dart';
import 'package:maktabeh_app/core/style/baseColors.dart';
import 'package:maktabeh_app/ui/about_us/about_us_screen.dart';
import 'package:maktabeh_app/ui/auth/LoginScreen.dart';
import 'package:maktabeh_app/ui/auth/SignupScreen.dart';
import 'package:maktabeh_app/ui/auth/resetPass/confairmEmailScreen.dart';
import 'package:maktabeh_app/ui/auth/resetPass/newPasswordScreen.dart';
import 'package:maktabeh_app/ui/book/about_writer.dart';
import 'package:maktabeh_app/ui/book/all_books.dart';
import 'package:maktabeh_app/ui/book/book_screen.dart';
import 'package:maktabeh_app/ui/book/books_writer.dart';
import 'package:maktabeh_app/ui/book/buy_books.dart';
import 'package:maktabeh_app/ui/book/quotes_screen.dart';
import 'package:maktabeh_app/ui/contact_us/contact_us_screen.dart';
import 'package:maktabeh_app/ui/guide/guide_screen.dart';

import 'package:maktabeh_app/ui/language_page/language_screen.dart';
import 'package:maktabeh_app/ui/mainScreens/HomSereens/HomeScreen.dart';
import 'package:maktabeh_app/ui/mainScreens/NotificationScreen.dart';
import 'package:maktabeh_app/ui/mainScreens/OuthorsScreen.dart';
import 'package:maktabeh_app/ui/mainScreens/ProfileScreen.dart';
import 'package:maktabeh_app/ui/mainScreens/main_screen.dart';
import 'package:maktabeh_app/ui/mainScreens/searchScreen.dart';
import 'package:maktabeh_app/ui/review/add_review_screen.dart';
import 'package:maktabeh_app/ui/review/review_screen.dart';
import 'package:maktabeh_app/ui/splash_screen/splash_screen.dart';

// import 'file:///E:/Android%20Projects/flutter_app/almashreq/lib/ui/guide/first_screen.dart';
import 'package:maktabeh_app/ui/start_screen/start_screen.dart';
import 'package:maktabeh_app/ui/user/editProfile.dart/editProfileScreen.dart';
import '../injection.dart';
import 'bloc/app_bloc.dart';
import 'bloc/app_state.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

// class App extends StatefulWidget {
//   App({Key key}) : super(key: key);
//
//   @override
//   _AppState createState() => _AppState();
// }
//
// class _AppState extends State<App> {
//   // ignore: close_sinks
//   final _bloc = sl<AppBloc>();
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return BlocBuilder(
//         bloc: _bloc,
//         builder: (BuildContext context, AppState state) {
//           print('Language App : ${state.appLanguage}');
//           return StreamBuilder(
//               stream: localeSubjectAppLanguage.stream.distinct(),
//               initialData: state.appLanguage == AppLanguageKeys.AR
//                   ? Locale('ar', '')
//                   : Locale('en', ''),
//               builder: (context, snapshotLanguage) {
//                 return MaterialApp(
//                     title: "Warehouses",
//                     home: Scaffold(
//                       backgroundColor: primaryColor,
//                     ),
//                     locale: snapshotLanguage.data == AppLanguageKeys.AR
//                         ? Locale('ar', '')
//                         : Locale('en', ''),
//                     localizationsDelegates: [
//                       AppLocalizations.delegate,
//                       GlobalMaterialLocalizations.delegate,
//                       GlobalWidgetsLocalizations.delegate,
//                     ],
//                     supportedLocales: [
//                       const Locale('en', ''), // English
//                       const Locale('ar', ''), // Arabic
//                     ]);
//               });
//         });
//   }
// }

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
      theme: ThemeData(
        primaryColor: primaryColor,
        accentColor: seconderyColor,
      ),
    );
  }
}
