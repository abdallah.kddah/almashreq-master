import 'package:flutter/material.dart';
import 'package:maktabeh_app/core/config/navigatorHelper.dart';
import 'package:maktabeh_app/core/style/baseColors.dart';
import 'package:maktabeh_app/ui/common_widget/CustomAlert.dart';
import 'package:maktabeh_app/ui/common_widget/customAppBar.dart';
import 'package:maktabeh_app/ui/paymentPages/typsPage.dart';

import 'component/SubscriptionCard.dart';

class SubscriptionScreen extends StatefulWidget {
  @override
  _SubscriptionScreenState createState() => _SubscriptionScreenState();
}

class _SubscriptionScreenState extends State<SubscriptionScreen> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: customAppBar(context, "الاشتراك"),
        body: ListView(
          children: [
            Container(
              color: seconderyColor,
              padding: EdgeInsets.symmetric(vertical: 15),
              child: Text(
                "متبقي لاشتراكي 15 يوم",
                style: boldStyle.copyWith(color: Colors.white),
              ),
              alignment: Alignment.center,
            ),
            Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "الاشتراك عن طريق الباقات",
                    style: boldStyle,
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SubscriptionCard(
                        onTap: () => push(context, TypesPage()),
                        title: "اشتراك شهري",
                        subTitle: "بقيمة 5 جنية",
                      ),
                      SubscriptionCard(
                        onTap: () => push(context, TypesPage()),
                        title: "اشتراك سنوي",
                        subTitle: "بقيمة 50 جنية",
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 20),
                    child: Row(
                      children: [
                        Text("الاشتراك عن طريق ", style: boldStyle),
                        Text(
                          "Promo Code",
                          style:
                              boldStyle.copyWith(fontWeight: FontWeight.w900),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Center(
                    child: SubscriptionCard(
                      onTap: () {
                        CustomAlert().submitChangeData(
                          context: context,
                          onSubmite: () {
                            Navigator.pop(context);
                            CustomAlert().successfulProcess(
                              btnText: "تصفح الكتب",
                              content:
                                  "تم عملية تفعيل البرمو كود يمكنك الان تصفح الكتب",
                              action: () {},
                              context: context,
                              title: "تم التفعيل",
                            );
                          },
                          textBtn: "تفعيل",
                          title: "يجب كتابة رمز البرومو كود هنا",
                        );
                      },
                      title: "اشتراك مجاني",
                      subTitle: "يتم ارسال الكود الي الايميل الخاص بكم",
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
