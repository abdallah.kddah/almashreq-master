import 'package:flutter/material.dart';
import 'package:maktabeh_app/core/style/baseColors.dart';
import 'package:maktabeh_app/ui/auth/compnent/CustomField2.dart';
import 'package:maktabeh_app/ui/common_widget/app_button.dart';
import 'package:maktabeh_app/ui/common_widget/customAppBar.dart';

class ContactusPage extends StatefulWidget {
  @override
  _ContactusPageState createState() => _ContactusPageState();
}

class _ContactusPageState extends State<ContactusPage> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: customAppBar(context, "تواصل معنا"),
        body: Column(
          children: [
            Container(
              width: double.infinity,
              padding: EdgeInsets.all(15),
              child: Text(
                "ملاحظه : سيتم الرد عليكم خلال 24 ساعه",
                style: regStyle.copyWith(color: Colors.white),
              ),
              color: seconderyColor,
            ),
            Expanded(
              child: ListView(
                padding: EdgeInsets.all(10),
                children: [
                  Text(
                    "تواصل معنا",
                    style: boldStyle.copyWith(fontSize: 15),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  CustomFeild2(
                    hintText: "الاسم الكريم",
                  ),
                  CustomFeild2(
                    hintText: "البريد الالكتروني",
                  ),
                  CustomFeild2(
                    lines: 6,
                    hintText: "رسالتك",
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  appButton(
                    buttonColor: primaryColor,
                    context: context,
                    onTap: () {},
                    text: "ارسال الرسالة",
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
