import 'package:flutter/material.dart';
import 'package:maktabeh_app/core/config/navigatorHelper.dart';
import 'package:maktabeh_app/core/size_config.dart';
import 'package:maktabeh_app/core/style/baseColors.dart';
import 'package:maktabeh_app/ui/auth/LoginScreen.dart';
import 'package:maktabeh_app/ui/auth/SignupScreen.dart';
import 'package:maktabeh_app/ui/common_widget/app_button.dart';
import 'package:maktabeh_app/ui/mainScreens/HomSereens/HomeScreen.dart';
import 'package:maktabeh_app/ui/mainScreens/main_screen.dart';

class StartScreen extends StatefulWidget {
  @override
  _StartScreenState createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  var isLargeScreen = false;
  bool isSelected = false;

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);

    if (MediaQuery.of(context).size.height > 700) {
      isLargeScreen = true;
    } else {
      isLargeScreen = false;
    }
    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          leading: Container(),
          actions: [
            InkWell(
              onTap: () => push(context, MainPage()),
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: SizeConfig.blockSizeHorizontal * 2),
                child: Row(
                  children: [
                    InkWell(
                      onTap: () => push(context, MainPage()),
                      child: Text(
                        'تخطي دخول',
                        style: boldStyle.copyWith(color: Colors.white),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: SizeConfig.blockSizeHorizontal * 2),
                      child: Image.asset('assets/image/arrowback_icon.png'),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: SizeConfig.screenHeight,
              child: Stack(
                children: [
                  Image.asset(
                    'assets/image/start.png',
                    width: SizeConfig.screenWidth,
                    height: SizeConfig.screenHeight * 0.65,
                    fit: BoxFit.fill,
                  ),
                  Column(
                    children: [
                      SizedBox(height: SizeConfig.screenHeight * 0.15),
                      Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          'assets/image/logo_image.png',
                          height: isLargeScreen
                              ? SizeConfig.screenHeight * 0.3
                              : SizeConfig.screenHeight * 0.23,
                        ),
                      ),
                    ],
                  ),
                  Positioned(
                    height: isLargeScreen
                        ? SizeConfig.screenHeight * 0.35
                        : SizeConfig.screenHeight * 0.4,
                    width: SizeConfig.screenWidth,
                    bottom: 0,
                    child: Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'يرجى تسجيل الدخول او تسجيل حساب',
                            style: boldStyle.copyWith(fontSize: 14),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                top: SizeConfig.blockSizeVertical * 3,
                                bottom: SizeConfig.blockSizeVertical * 2),
                            child: appButton(
                              buttonColor: primaryColor,
                              context: context,
                              text: "تسجيل دخول",
                              textColor: Colors.white,
                              onTap: () =>
                                  Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => LoginScreen(),
                              )),
                            ),
                          ),
                          Container(
                            width: SizeConfig.screenWidth * 0.9,
                            height: SizeConfig.screenHeight * 0.06,
                            child: FlatButton(
                                color: Colors.white,
                                onPressed: () => Navigator.of(context)
                                        .push(MaterialPageRoute(
                                      builder: (context) => SignupScreen(),
                                    )),
                                shape: RoundedRectangleBorder(
                                  side:
                                      BorderSide(width: 1, color: primaryColor),
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                child: Text('تسجيل جديد',
                                    style: regStyle.copyWith(
                                        fontSize: 14, color: primaryColor))),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: SizeConfig.blockSizeVertical * 2),
                            child: Text(
                              'او التسجيل بإستخدام',
                              style: regStyle.copyWith(
                                  color: Color(0xFFD4D4D4),
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                width: 50,
                                height: 50,
                                child: Image.asset(
                                    'assets/image/facebook_icon.png'),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: seconderyColor,
                                ),
                              ),
                              SizedBox(
                                  width: SizeConfig.blockSizeHorizontal * 2),
                              Container(
                                width: 50,
                                height: 50,
                                child:
                                    Image.asset('assets/image/google_icon.png'),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: seconderyColor,
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ));
  }
}
